const dotenv = require('dotenv');
const result = dotenv.config();


if(result.error) {
console.log(result.error);

} else {

    module.exports = {
        port:process.env.PORT
    }
}

