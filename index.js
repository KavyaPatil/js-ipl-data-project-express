const requestId = require("express-request-id");
const express = require("express");
const { port } = require("./config");
const fs = require("fs");
const http = require("http");
const path = require("path");
const logRequestMiddleware = require("./middleware/logRequest");
const errorHandlerMiddleware = require("./middleware/errorHandler");
const unexpectedRequestHandler = require("./middleware/unexpectedRequestHandler");
const iplRouter = require("./routers/iplRouter");
const logFilePath = path.join(__dirname, "../request.log");
const router = require("./routers/router");

const app = express();

function workingWIthIplUsingExpress() {


  app.use(requestId());

  app.use(logRequestMiddleware);

  app.use(router);

  app.use("/api", iplRouter);

  app.use(unexpectedRequestHandler);

  app.use(errorHandlerMiddleware);

  app.listen(port, () => {
    console.log("on Live");
  });
  
}

workingWIthIplUsingExpress();
