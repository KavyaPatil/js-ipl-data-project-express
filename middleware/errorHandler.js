

function errorHandlerMiddleware(err, req, res, next){
    if(err) {
      console.log(err);
      res.status(500).send("Internal Server Error")
    }
  }


  module.exports = errorHandlerMiddleware;