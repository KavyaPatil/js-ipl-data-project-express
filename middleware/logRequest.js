const path = require('path');
const fs = require('fs');
const logFilePath = path.join(__dirname, "../request.log");



function logRequestMiddleware(req, res, next) {    //defined the logRequestMiddleware function
    
    const logDetails = `date:${new Date().toISOString()}  method:${req.method}  url:${req.url}   requestID : ${req.id} \n`;

    fs.appendFile(logFilePath, (logDetails), (err) => {
      if (err) {
        next(err);
      }
    });

    next();
  }


  module.exports = logRequestMiddleware;
