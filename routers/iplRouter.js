const express = require('express');
const router = express.Router();
const path = require("path");
const matchesPerYear = require("../src/server/1-matches-per-year.cjs");
const matchesWonPerTeamPerYear = require("../src/server/2-matches-won-per-team-per-year.cjs")
const extraRunsPerTeamIn2016 = require("../src/server/3-extra-runs-per-team-in-2016.cjs");
const topTenEconomicBowlersIn2015 = require("../src/server/4-top-ten-economic-bowlers-2015.cjs");
const numberOfTimesTeamWonTossMatch = require("../src/server/5-number-of-times-each-team-won-toss-won-match.cjs");
const highestManOfTheMatchEachYear = require("../src/server/6-highest-man-of-the-match-each-year.cjs");
const strikeRateOfBatsmanEachYear = require("../src/server/7-strike-rate-of-batsman-for-each-season.cjs");
const playerDismissedByPlayer = require("../src/server/8-highest-number-of-times-player-dismissed-by-player.cjs");
const bestBowlerInSuperOvers = require("../src/server/9-best-bowler-with-best-economy-in-super-overs.cjs");




router.get("/", (req, res, next) => {
    
    res.sendFile(path.join(__dirname, "../homePage.html"));

} )

router.get("/1-match-per-year", (req, res, next) => {
    
    matchesPerYear().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )

router.get("/2-matches-won-per-team-per-year", (req, res, next) => {
    
    matchesWonPerTeamPerYear().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })
} )

router.get("/3-extra-runs-per-team-in-2016", (req, res, next) => {
    
    extraRunsPerTeamIn2016().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )

router.get("/4-top-ten-economic-bowlers-2015", (req, res, next) => {
    
    topTenEconomicBowlersIn2015().then((data) => {
       
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )


router.get("/5-number-of-times-each-team-won-toss-won-match", (req, res, next) => {
    
    numberOfTimesTeamWonTossMatch().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )

router.get("/6-highest-man-of-the-match-each-year", (req, res, next) => {
    
    highestManOfTheMatchEachYear().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )

router.get("/7-strike-rate-of-batsman-for-each-season", (req, res, next) => {
    
    strikeRateOfBatsmanEachYear().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )

router.get("/8-highest-number-of-times-player-dismissed-by-player", (req, res, next) => {
    
    playerDismissedByPlayer("MS Dhoni").then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )

router.get("/9-best-bowler-with-best-economy-in-super-overs", (req, res, next) => {
    
    bestBowlerInSuperOvers().then((data) => {
        res.json(data).end();
     })
     .catch((err) => {
        next(err)
     })

} )










module.exports = router;