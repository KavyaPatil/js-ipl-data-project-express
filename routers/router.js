const path = require('path');
const logFilePath = path.join(__dirname, "../request.log");
const express = require('express');
const router = express.Router();


router.get("/", (req, res, next) => {
    res.sendFile(path.join(__dirname, "../home.html"));
})

router.get("/logs", (req, res, next) => {
    res.sendFile(logFilePath);
})



module.exports = router;