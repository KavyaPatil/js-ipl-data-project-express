const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvFilePath = path.join(__dirname, "../data/matches.csv")




async function matchesPerYearFunction() {

  const data = await csvtojson().fromFile(csvFilePath);

  const matchesPerYear = data.reduce((newObj, key) => {

    if (key.season in newObj) {

      newObj[key.season] += 1;

    } else {

      newObj[key.season] = 1;
    }

    return newObj;

  }, {});      //initialising new object and storing the data in that object using reduce

  return matchesPerYear;
}

module.exports = matchesPerYearFunction;

