const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvFilePath = path.join(__dirname, "../data/matches.csv")



async function matchesWonPerTeamFunction() {

  const data = await csvtojson().fromFile(csvFilePath);

  const matchesWon = data.reduce((newObj, ele) => {

    if (ele.season in newObj) {

      if (ele.winner in newObj[ele.season]) {
        newObj[ele.season][ele.winner] += 1;

      } else {
        newObj[ele.season][ele.winner] = 1;

      }
    } else {
      newObj[ele.season] = {};
      newObj[ele.season][ele.winner] = 1;

    }

    return newObj;

  }, {});

  console.log(matchesWon);

  return matchesWon;
}

module.exports = matchesWonPerTeamFunction;