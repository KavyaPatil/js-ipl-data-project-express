const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvMatchFilePath = path.join(__dirname, "../data/matches.csv")
const csvDeliveriesFilePath = path.join(__dirname, "../data/deliveries.csv")




async function extraRunsConcededFunction() {

  const matcheData = await csvtojson().fromFile(csvMatchFilePath);
  const deliveriesData = await csvtojson().fromFile(csvDeliveriesFilePath);

  const data_2016 = matcheData
    .filter((item) => {
      return item.season === "2016";
    })
    .map((ele) => {
      return ele.id;
    });


  let extraRuns = deliveriesData.reduce((newObj, item) => {

    if (data_2016.includes(item.match_id)) {

      if (item.bowling_team in newObj) {
        newObj[item.bowling_team] += +item.extra_runs;

      } else {
        newObj[item.bowling_team] = +item.extra_runs;

      }
    }
    return newObj;
    
  }, {});

  console.log(extraRuns);

  return extraRuns;

}



module.exports = extraRunsConcededFunction;
