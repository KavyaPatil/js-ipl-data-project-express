const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvFilePath = path.join(__dirname, "../data/matches.csv")
const csvDeliveriesFilePath = path.join(__dirname, "../data/deliveries.csv")


async function topTenEconomicBowlerFunction() {

  const deliveriesData = await csvtojson().fromFile(csvDeliveriesFilePath);
  const matchData = await csvtojson().fromFile(csvFilePath);

  let data_2015 = matchData
    .filter((item) => {
      return item.season === "2015";
    })
    .map((element) => {
      return element.id;
    });

    // console.log(matchData)
  const economicBowlers = deliveriesData.reduce((newObj, item) => {

    if (data_2015.includes(item.match_id)) {
       
      if (item.bowler in newObj) {
      
        if (+item.ball > 6) {
          newObj[item.bowler].runs += +item.total_runs;

        } else {
          newObj[item.bowler].balls += 1;
          newObj[item.bowler].runs += +item.total_runs;

        }
      } else {

        if (item.extra_runs !== "0") {
          newObj[item.bowler] = {};
          newObj[item.bowler].balls = 0;
          newObj[item.bowler].runs = +item.total_runs;

        } else {
          newObj[item.bowler] = {};
          newObj[item.bowler].balls = 1;
          newObj[item.bowler].runs = +item.total_runs;
        }
      }
    }

    return newObj;
  }, {});

  // console.log(economicBowlers);

  let entries = Object.entries(economicBowlers).sort((bowler1, bowler2) => {
      const bowlerEconomy1 = Math.floor((bowler1[1].runs / bowler1[1].balls) * 6);
      const bowlerEconomy2 = Math.floor((bowler2[1].runs / bowler2[1].balls) * 6);
      
      return bowlerEconomy1 - bowlerEconomy2;

    })
    .slice(0, 10);

  // console.log(entries);

  // const topTenEconomicBowlers = Object.fromEntries(entries);

  const result = entries.reduce((newObj, element) => {

console.log(element);
newObj[element[0]] = Math.floor((element[1].runs/element[1].balls)*6);

return newObj;

  }, {})
  // console.log(topTenEconomicBowlers)
  return result;
}



module.exports = topTenEconomicBowlerFunction;

