const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvMatchFilePath = path.join(__dirname, "../data/matches.csv")



async function teamWonTossWonMatchFunction() {
  
  const matcheData = await csvtojson().fromFile(csvMatchFilePath);

  const teamWonTossWonMatch = matcheData.reduce((newObj, element) => {

    if (element.toss_winner === element.winner) {

      if (element.winner in newObj) {
        newObj[element.winner] += 1;

      } else {
        newObj[element.winner] = 1;
      }
    }

    return newObj;

  }, {});

  console.log(teamWonTossWonMatch);

  return teamWonTossWonMatch;
}



module.exports = teamWonTossWonMatchFunction;

