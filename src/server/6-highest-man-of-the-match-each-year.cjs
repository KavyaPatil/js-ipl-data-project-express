const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvFilePath = path.join(__dirname, "../data/matches.csv")



async function manOfTheMatchFunction() {

  const data = await csvtojson().fromFile(csvFilePath);

  const manOfTheMatch = data.reduce((newObj, element) => {
    if (element.season in newObj) {
      if (element.player_of_match in newObj[element.season]) {
        newObj[element.season][element.player_of_match] += 1;

      } else {
        newObj[element.season][element.player_of_match] = 1;

      }
    } else {
      newObj[element.season] = {};
      newObj[element.season][element.player_of_match] = 1;

    }

    return newObj;
  }, {});

 
  const arrayOfManOfTheMatch = Object.keys(manOfTheMatch).map((season) => {
   const arrayOfPlayers =  Object.entries(manOfTheMatch[season]).sort((player1, player2) => {

      return player2[1] - player1[1];

    });

    return arrayOfPlayers[0];

  });


  return Object.fromEntries(arrayOfManOfTheMatch);
}



module.exports = manOfTheMatchFunction;
