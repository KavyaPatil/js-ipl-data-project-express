const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvMatchFilePath = path.join(__dirname, "../data/matches.csv")
const csvDeliveriesFilePath = path.join(__dirname, "../data/deliveries.csv")


async function strikeRateFunction() {
  
  const matcheData = await csvtojson().fromFile(csvMatchFilePath);
  const deliveriesData = await csvtojson().fromFile(csvDeliveriesFilePath);

  const strikeBallsruns = deliveriesData.reduce((newObj, element) => {
    const yearStrikeRate = matcheData.find((item) => {
      return item.id === element.match_id;
    });

    const year = yearStrikeRate.season;

    if (element.batsman === "MS Dhoni" && element.batsman in newObj) {
      if (year in newObj[element.batsman]) {
        newObj[element.batsman][year].balls += 1;
        newObj[element.batsman][year].runs += +element.batsman_runs;
      } else {
        newObj[element.batsman][year] = {};
        newObj[element.batsman][year].balls = 1;
        newObj[element.batsman][year].runs = +element.batsman_runs;
      }
    } else if (
      element.batsman === "MS Dhoni" &&
      element.batsman in newObj == false
    ) {
      newObj[element.batsman] = {};
      newObj[element.batsman][year] = {};
      newObj[element.batsman][year].balls = 1;
      newObj[element.batsman][year].runs = +element.batsman_runs;
    }

    return newObj;
  }, {});

  const strikeRate = Object.entries(strikeBallsruns).reduce((newObj, element) => {

      const arrayOfObjects = Object.keys(element[1]).map((season) => {
        const obj = {};

        obj[season] = Math.floor((element[1][season].runs / element[1][season].balls) * 100);
        console.log(obj);

        return obj;

      });

      newObj[element[0]] = arrayOfObjects;

      return newObj;
    },
    {}
  );

  return strikeRate;
}

module.exports = strikeRateFunction;

