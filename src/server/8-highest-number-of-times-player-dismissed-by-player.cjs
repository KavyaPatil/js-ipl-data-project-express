const path = require('path');
const csvtojson = require("csvtojson");
const csvFilePath = path.join(__dirname, "../data/deliveries.csv")



async function playerDismissedFunction() {

  const data = await csvtojson().fromFile(csvFilePath);

  const dismissedPlayer = data
    .filter((item) => {
      return item.player_dismissed !== "";
      
    })
    .reduce((newObj, element) => {
      if (element.batsman in newObj) {
        if (element.bowler in newObj[element.batsman]) {
          newObj[element.batsman][element.bowler] += 1;

        } else {
          newObj[element.batsman][element.bowler] = 1;

        }
      } else {
        newObj[element.batsman] = {};
        newObj[element.batsman][element.bowler] = 1;

      }
      return newObj;
    }, {});

  // console.log(dismissedPlayer);

  const arrayOfObjects = Object.keys(dismissedPlayer).map((player) => {
    const object = {};

    const arrayOfPlayers = Object.entries(dismissedPlayer[player]).sort( (bowler1, bowler2) => {
        return bowler2[1] - bowler1[1];

      }
    );

    object[player] = arrayOfPlayers[0];

    return object;
  });
  

  let max = 0;
  const result = arrayOfObjects.reduce((newObj, element) => {
    let key = Object.keys(element);

    if (element[key][1] > max) {
      max = element[key][1];
      newObj = { ...element };

    } else {
      newObj = { ...newObj };
    }

    return newObj;
  }, {});



  return result;
}


module.exports = playerDismissedFunction;
