const fs = require("fs");
const path = require('path');
const csvtojson = require("csvtojson");
const csvFilePath = path.join(__dirname, "../data/deliveries.csv")



async function topEconomicBowlerFunction() {
  const jsonArray = await csvtojson().fromFile(csvFilePath);

  let data = jsonArray.filter((item) => {
    return item.is_super_over !== "0";

  });

  const bowlers = data.reduce((newObj, element) => {

    if (element.bowler in newObj) {
      newObj[element.bowler] += +element.total_runs;

    } else {
      newObj[element.bowler] = +element.total_runs;

    }

    return newObj;

  }, {});

  let min = Infinity;
  let topEconomicBowler;
  console.log

  const bestEconomy = Object.keys(bowlers).reduce((newObj, bowler) => {


    console.log(bowler);
    if (bowlers[bowler] < min) {
      min = bowlers[bowler];
      topEconomicBowler = bowler;
       
      newObj.bowler = topEconomicBowler;
      newObj.economy = min;

    } else {
      newObj = { ...newObj };

    }

    return newObj;

  }, {});

// console.log(bestEconomy);
  return bestEconomy;
}

module.exports = topEconomicBowlerFunction;

